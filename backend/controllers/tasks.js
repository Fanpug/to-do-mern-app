const express = require('express');
//const config = require('config');
const Task = require("../models/task");


function list(req, res, next) {
    //let page = req.params.page ? req.params.page : 1;
    const filter = {};
    Task.find(filter)
    .then(obj => res.status(200).json({
        message: 'ok.find',
        obj: obj
    }))
    .catch(ex => res.status(500).json({
        message: 'No se pudo hacer eso list',
        obj: ex
    }));
}

function index(req, res, next){
    const id = req.params.id;

    Task.findOne({"_id":id})
    .then(obj => res.status(200).json({
        message: 'ok.find',
        obj: obj
    }))
    .catch(ex => res.status(500).json({
        message: 'No se pudo hacer eso get 1',
        obj: ex
    }));

}

function create(req, res, next){
    const description = req.body.description;
    const completed = req.body.completed;

    let task = new Task({
        description:description,
        completed:completed
    });

    task.save()
    .then(obj => res.status(200).json({
        message: 'Se creó un task',
        obj: obj
    }))
    .catch(ex => res.status(500).json({
        message: 'No se pudo hacer eso create',
        obj: ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    let description = req.body.description ? req.body.description : "";
    let completed = req.body.completed ? req.body.completed : "";

    let task = new Object({
        _description: description,
        _completed: completed
    });

    Task.findOneAndUpdate({"_id":id}, task)
    .then(obj => res.status(200).json({
        message: 'ok.replace',
        obj: obj
    }))
    .catch(ex => res.status(500).json({
        message: 'No se pudo hacer eso replace',
        obj: ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let description = req.body.description;
    let completed = req.body.completed;

    let task = new Object();

    if(description){
        task._description = description;
    }

    if(completed){
        task._completed = completed;
    }

    Task.findOneAndUpdate({"_id":id}, task)
    .then(obj => res.status(200).json({
        message: 'ok.edit',
        obj: obj
    }))
    .catch(ex => res.status(500).json({
        message: 'No se pudo hacer eso edit',
        obj: ex
    }));
}

function destroy(req, res, next){
    const id = req.params.id;

    Task.remove({"_id":id})
    .then(obj => res.status(200).json({
        message: 'ok.destroy',
        obj: obj
    }))
    .catch(ex => res.status(500).json({
        message: 'No se pudo hacer eso delete',
        obj: ex
    }));
}


module.exports = {
    list, index, create, replace, edit, destroy
}