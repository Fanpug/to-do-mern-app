const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _description:String,
    _completed:Boolean
});

class Task {
    constructor(description, completed){
        this._description = description;
        this._completed = completed;
    }

    get description(){
        return this._description;
    }

    set description(value){
        this._description = value;
    }

    get completed(){
        return this._completed;
    }

    set completed(value){
        this._completed = value;
    }
}

schema.loadClass(Task);
module.exports = mongoose.model('Task', schema);