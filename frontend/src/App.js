import './App.css';

import { useState, useEffect } from 'react';


//import data from './data.json';
import TodoList from './components/TodoList';
import TodoForm from './components/TodoForm';

function App() {
  const [todos , setTodos] = useState([]);

  useEffect(() => {
    const url = "http://localhost:8080/";

    const fetchTodos = async () => {
      const response = await fetch(url);
      const jsonObj = await response.json();

      if(response.ok){
        setTodos(jsonObj);
      }
      console.log("fetch aqui 1 " + jsonObj);
      for(var property in jsonObj.obj) {
        alert(property + "=" + jsonObj.obj[property]);

      }
      setTodos(jsonObj.obj);
    };

    fetchTodos();

  }, []);





  const onComplete = (id) => {
    setTodos(
    //Vamos a iterar en todos los todos
    todos.map((todo) => {
      //Si el id de algun task es igual al que estamos recibiendo entonces actualiza su propiedad completed
      return todo.id === Number(id) ? {...todo, completed: !todo.completed} : {...todo}
    }))
  }

  const onDeleteItem = (id) => {
    //Si algun todo tiene el id diferente al id que estamos recibiendo entonces vamos a utilizarlo
    setTodos([...todos].filter(todo => todo.id !== id))
  }

  const addTodo = (newTodo) => {
    let newItem = {id: +new Date(), task: newTodo, completed: false};
    setTodos([...todos, newItem]); 
  }

  return (
    <div className="container">
      <TodoForm addTodo = {addTodo}/>
      <TodoList todos = {todos} onComplete = {onComplete} onDeleteItem={onDeleteItem}/>
    </div>
  );
}

export default App;
