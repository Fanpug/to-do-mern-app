const API_URL = 'http://localhost:8080/'

export default async () => {
    const response = await fetch(API_URL);
    return await JSON.parse(response);
}